# DT Backend course test task

# Почему это сделано именно так?
1. Я не знаю, что хотел сказать автор, выставив `AUTH_USER_MODEL = "app.AdminUser"` в settings.py,
    но я не придумал ничего лучше, кроме как скатиться в использование
    джанговской модели юзера для аутентификации, поэтому расширил эту модель
    вместо создания отдельной для пользователей.
2. Дальше я не придумал ничего лучше, чем генерить пользователю рандомный
    пароль, который далее будет использоваться для аутентификации в REST API.
3. А ещё я решил, что aiogram - это что-то модное, и напрочь забыл о том, что
    придётся потом дружить синк и асинк.

# Работа с проектом и запуск
Если не установлен pipenv, то надо поставить
```shell
pip install --user pipenv
```
Далее (как поставили или если уже был установлен),
в папке со склонированным проектом установить зависимости:
```shell
pipenv install
```
Чтобы активировать виртуальное окружение в текущем терминале:
```shell
pipenv shell
```
По аналогии с `.env.example` создайте файл `.env`
и заполните все необходимые параметры.

Примените миграции к БД:
```shell
make migrate
```
Теперь всё готово к запуску проекта.

Запуск веб-сервера, предоставляющего админку и REST API:
```shell
make dev
```
Запуск телеграм-бота:
```shell
make start_bot
```

P.S. Если вы работаете с Pycharm, нажмите правой кнопкой по папке `/src`
-> Mark directory as -> Sources root. Тогда Pycharm начнёт адекватно
воспринимать импорты.

# REST API
* **POST** /api/login/

Запрос:

**Body**
```json
{
  "username": "string",
  "password": "string"
}
```
Ответ:
```json
{
  "token": "your_auth_token"
}
```
* **GET** /api/me/

Запрос:

**Headers**
```
Authorization: Token your_auth_token
```
Ответ:
```json
{
    "first_name": "string",
    "telegram_id": 0,
    "username": "string"
}
```

# Telegram-бот команды:
* `/start` - сохраняет или обновляет информацию о пользователе в базу
* `/set_phone <phone_number>` - задаёт пользователю номер телефона
* `/me` - выдаёт информацию о пользователе
  (доступна только пользователям с заполненным номером телефона)