from aiogram.types import User
from django.db.utils import Error

from app.internal.models.admin_user import AdminUser
from app.internal.utils.errors import CommonErrors, ErrorObject


async def update_or_create_from_tg(tg_data: User) -> tuple[str | None, None] | tuple[None, ErrorObject]:
    try:
        try:
            await AdminUser.objects.aget(id=tg_data.id)
        except AdminUser.DoesNotExist:
            password = AdminUser.objects.make_random_password()
            new_user = AdminUser(
                id=tg_data.id,
                first_name=tg_data.first_name,
                last_name=tg_data.last_name,
                username=tg_data.username,
            )
            new_user.set_password(password)
            await new_user.asave()
            return password, None
        await AdminUser.objects.filter(id=tg_data.id).aupdate(
            first_name=tg_data.first_name, last_name=tg_data.last_name, username=tg_data.username
        )
        return None, None
    except Error:
        return None, CommonErrors.get_db_error()


async def set_phone(user_id: int, phone: str) -> None | ErrorObject:
    try:
        await AdminUser.objects.filter(id=user_id).aupdate(phone=phone)
    except Error:
        return CommonErrors.get_db_error()
