from enum import Enum

from pydantic import BaseModel


class ErrorObject(BaseModel):
    type: str
    message: str


class CommonErrorTypes(str, Enum):
    DB_ERROR = "db_error"


class CommonErrors:
    @staticmethod
    def get_db_error() -> ErrorObject:
        return ErrorObject(
            type=CommonErrorTypes.DB_ERROR,
            message="Something went wrong during accessing the database",
        )
