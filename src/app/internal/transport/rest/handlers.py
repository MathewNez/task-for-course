from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


class MeView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        content = {
            "first_name": request.user.first_name,
            "telegram_id": request.user.pk,
            "username": request.user.username,
        }

        return Response(content)
