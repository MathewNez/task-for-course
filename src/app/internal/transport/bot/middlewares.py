from typing import Any, Awaitable, Callable

from aiogram import BaseMiddleware
from aiogram.types import Message, TelegramObject
from django.db.utils import Error

from app.internal.models.admin_user import AdminUser


class GetUserMiddleware(BaseMiddleware):
    async def __call__(
        self, handler: Callable[[TelegramObject, dict[str, Any]], Awaitable[Any]], event: Message, data: dict[str, Any]
    ) -> Any:
        try:
            user = await AdminUser.objects.aget(id=event.from_user.id)
        except AdminUser.DoesNotExist:
            user = None
        except Error:
            await event.answer("Something went wrong. Please, try again later")
            return
        data["user"] = user
        return await handler(event, data)


class CheckUserPhoneMiddleware(BaseMiddleware):
    async def __call__(
        self, handler: Callable[[TelegramObject, dict[str, Any]], Awaitable[Any]], event: Message, data: dict[str, Any]
    ) -> Any:
        user: AdminUser | None = data.get("user")
        if not user or not user.phone:
            await event.answer("You need to set your phone number " "via /set_phone command before taking next steps")
            return
        return await handler(event, data)
