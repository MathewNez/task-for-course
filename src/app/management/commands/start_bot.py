import asyncio

from django.core.management import BaseCommand

from app.internal.bot import run_bot


class Command(BaseCommand):
    def handle(self, *args, **options):
        asyncio.run(run_bot())
