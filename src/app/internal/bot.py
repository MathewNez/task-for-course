from aiogram import Bot, Dispatcher
from aiogram.enums import ParseMode
from django.conf import settings

from app.internal.transport.bot.handlers import protected_commands_router, public_commands_router
from app.internal.transport.bot.middlewares import GetUserMiddleware


async def run_bot() -> None:
    dp = Dispatcher()

    dp.message.middleware(GetUserMiddleware())
    dp.include_router(public_commands_router)
    dp.include_router(protected_commands_router)

    bot = Bot(settings.TG_BOT_TOKEN, parse_mode=ParseMode.HTML)

    await dp.start_polling(bot)
