from django.urls import include, path
from rest_framework.authtoken import views

from app.internal.transport.rest.handlers import MeView

urlpatterns = [
    path("login/", views.obtain_auth_token),
    path("me/", MeView.as_view()),
]
