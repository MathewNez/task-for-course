from aiogram import Router
from aiogram.filters import Command
from aiogram.types import Message

from app.internal.models.admin_user import AdminUser
from app.internal.services.user_service import set_phone, update_or_create_from_tg
from app.internal.transport.bot.middlewares import CheckUserPhoneMiddleware
from app.internal.utils import regexps

public_commands_router = Router()


@public_commands_router.message(Command("start"))
async def command_start_handler(message: Message) -> None:
    password, error = await update_or_create_from_tg(message.from_user)
    if error:
        await message.answer("Something went wrong. Please, try again later")
        return
    if not password:
        await message.answer(f"Nice to meet you again, {message.from_user.first_name}!")
        return
    await message.answer(
        f"Hello, {message.from_user.first_name}! "
        f"Your password for external services is {password} . "
        f"Keep it in secret and please change as soon as we add such functionality"
    )


@public_commands_router.message(Command("set_phone"))
async def command_set_phone_handler(message: Message, user: AdminUser):
    try:
        phone = message.text.split(" ")[1]
    except IndexError:
        phone = ""
    if not regexps.PHONE_REGEX.match(phone):
        await message.answer(
            "This does not seem to be a phone number.\n"
            "Usage:\n"
            "/set_phone 88005553535\n"
            "or\n"
            "/set_phone +78005553535"
        )
        return

    if _ := await set_phone(user.pk, phone):
        await message.answer("Something went wrong. Please, try again later")
        return

    await message.answer("Successfully set phone number to your account")


protected_commands_router = Router()
protected_commands_router.message.middleware(CheckUserPhoneMiddleware())


@protected_commands_router.message(Command("me"))
async def command_me_handler(message: Message, user: AdminUser):
    await message.answer(
        f"You are {user.first_name}\n" f"With Telegram id {user.pk}\n" f"And a username {user.username}"
    )
